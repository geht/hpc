// hpc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
ppm documentation: http://netpbm.sourceforge.net/doc/ppm.html

Each PPM image consists of the following:

A "magic number" for identifying the file type. A ppm image's magic number is the two characters "P6".
Whitespace (blanks, TABs, CRs, LFs).
A width, formatted as ASCII characters in decimal.
Whitespace.
A height, again in ASCII decimal.
Whitespace.
The maximum color value (Maxval), again in ASCII decimal. Must be less than 65536 and more than zero.
A single whitespace character (usually a newline).
A raster of Height rows, in order from top to bottom.
	Each row consists of Width pixels, in order from left to right.
	Each pixel is a triplet of red, green, and blue samples, in that order.
	Each sample is represented in pure binary by either 1 or 2 bytes.
	If the Maxval is less than 256, it is 1 byte.
	Otherwise, it is 2 bytes.
	The most significant byte is first.
*/

void saveImage(const glm::vec3 * image, const char * path)
{
	// print the pixels out to ppm file
	std::ofstream ofs(path, std::ios::out | std::ios::binary);
	ofs << "P6\n" << SCREEN_WIDTH << " " << SCREEN_HEIGHT << "\n255\n";

	for (int y = 0; y < SCREEN_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			glm::vec3 pixel = image[SCREEN_WIDTH * y + x];

			ofs <<	(unsigned char)(glm::min(float(1), pixel.x) * 255) <<
					(unsigned char)(glm::min(float(1), pixel.y) * 255) <<
					(unsigned char)(glm::min(float(1), pixel.z) * 255);
		}
	}

	ofs.close();
}

int main()
{
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point finish;
	double dif;
	glm::vec3 * image;

	printf("initializing raytracer...\n\n");
	RayTracer rayTracer;
	printf("raytracer initialized\n\n");

	printf("the scene will be rendered with a resolution of %i by %i pixels\n", SCREEN_WIDTH, SCREEN_HEIGHT);
	printf("supersampling: %i primary rays will be cast per pixel\n", SUPER_SAMPLES);
	printf("rays will be traced with a maximum trace depth of %i\n\n", MAX_TRACE_DEPTH);

	printf("rendering scene serially...\n");
	start = std::chrono::high_resolution_clock::now();
	image = rayTracer.renderSerial();
	finish = std::chrono::high_resolution_clock::now();
	dif = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();
	printf("finished rendering\n");
	printf("rendering took %f seconds\n\n", dif);

	printf("rendering scene parallel...\n");
	start = std::chrono::high_resolution_clock::now();
	image = rayTracer.renderParallel();
	finish = std::chrono::high_resolution_clock::now();
	dif = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();
	printf("finished rendering\n");
	printf("rendering took %f seconds\n\n", dif);

	printf("rendering scene parallel with parallel supersampling...\n");
	start = std::chrono::high_resolution_clock::now();
	image = rayTracer.renderParallel_ParallelSS();
	finish = std::chrono::high_resolution_clock::now();
	dif = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();
	printf("finished rendering\n");
	printf("rendering took %f seconds\n\n", dif);

	printf("saving the picture...\n");
	saveImage(image, FILE_PATH);
	printf("saved the picture as: %s\n\n", FILE_PATH);

	printf("cleaning up...\n");
	delete[] image;
	printf("done\n\n");

	// pause before quitting
	printf("press ENTER to quit...\n");
	std::cin.get();

    return 0;
}