#pragma once

struct Sphere : Primitive
{
	glm::vec3 center;
	float radius;

	Sphere(const glm::vec3 & a_center, float a_radius) :
		center(a_center), radius(a_radius)
	{
		isLight = false;
	}

	Sphere(float x, float y, float z, float a_radius) :
		center(glm::vec3(x, y, z)), radius(a_radius)
	{
		isLight = false;
	}

	int getType() override { return SPHERE; }

	int intersect(const Ray & ray, float & t) override
	{
		int retVal = MISS;
		glm::vec3 distance = center - ray.origin;
		float b = glm::dot(ray.direction, distance);
		float d = b * b - glm::dot(distance, distance) + radius * radius;

		if (d < 0.0f)
			return retVal;

		d = glm::sqrt(d);
		float t0 = b - d;
		float t1 = b + d;

		if (t1 > 0.0f)
		{
			if (t0 < 0.0f)
			{
				if (t1 < t)
				{
					t = t1;
					retVal = INPRIM;
				}
			}
			else
			{
				if (t0 < t)
				{
					t = t0;
					retVal = HIT;
				}
			}
		}

		return retVal;
	}

	glm::vec3 normal(const glm::vec3 & position, const glm::vec3 & direction) override
	{
		return ((position - center) * (1.0f / radius));
	}
};