#pragma once

struct ScreenPlane
{
	// screen plane in world space coordinates
	glm::vec3 corner1;
	glm::vec3 corner2;
	glm::vec3 delta;

	ScreenPlane()
	{
		corner1 = glm::vec3(-4.0f, 3.0f, 0.0f);
		corner2 = glm::vec3(4.0f, -3.0f, 0.0f);

		delta = corner2 - corner1;
		delta.x /= float(SCREEN_WIDTH);
		delta.y /= float(SCREEN_HEIGHT);
	}

	Ray constructRay(const glm::vec3 & origin, const glm::vec3 & coordinates)
	{
		glm::vec3 dir = corner1 + coordinates * delta;
		dir -= origin;
		dir = glm::normalize(dir);

		Ray ray;
		ray.origin = origin;
		ray.direction = dir;

		return ray;
	}

	Ray constructRay(const glm::vec3 & origin, int x, int y)
	{
		return constructRay(origin, glm::vec3(x, y, 0.0f));
	}

	Ray constructRay(const glm::vec3 & origin, int x, int y, int tx, int ty, int q)
	{
		glm::vec3 coordinates(x, y, 0.0f);
		coordinates += glm::vec3(tx, ty, 0.0f) * delta / (float)q;
		return constructRay(origin, coordinates);
	}
};