#pragma once

struct QuadricGeneric : Primitive
{
	// F(x, y, z) = Ax^2 + By^2 + Cz^2 + Dxy+ Exz + Fyz + Gx + Hy + Iz + J = 0
	float a, b, c, d, e, f, g, h, i, j;

	QuadricGeneric(float a_a, float a_b, float a_c, float a_d, float a_e, float a_f, float a_g, float a_h, float a_i, float a_j) :
		a(a_a), b(a_b), c(a_c), d(a_d), e(a_e), f(a_f), g(a_g), h(a_h), i(a_i), j(a_j)
	{
		isLight = false;
	}

	int getType() override { return QUADRIC_GENERIC; }

	int intersect(const Ray & ray, float & t) override
	{
		int retVal = MISS;
		float t0, t1;

		float Aq = a * ray.direction.x * ray.direction.x + b * ray.direction.y * ray.direction.y + c * ray.direction.z * ray.direction.z + d * ray.direction.x * ray.direction.y + e * ray.direction.x * ray.direction.z + f * ray.direction.y * ray.direction.z;
		float Bq = 2.0f * a * ray.origin.x * ray.direction.x + 2.0f * b * ray.origin.y * ray.direction.y + 2.0f * c * ray.origin.z * ray.direction.z + d * (ray.origin.x * ray.direction.y + ray.origin.y * ray.direction.x) + e * (ray.origin.x * ray.direction.z + ray.origin.z * ray.direction.x) + f * (ray.origin.y * ray.direction.z + ray.origin.z * ray.direction.y) + g * ray.direction.x + h * ray.direction.y + i * ray.direction.z;
		float Cq = a * ray.origin.x * ray.origin.x + b * ray.origin.y * ray.origin.y + c * ray.origin.z * ray.origin.z + d * ray.origin.x * ray.origin.y + e * ray.origin.x * ray.origin.z + f * ray.origin.y * ray.origin.z + g * ray.origin.x + h * ray.origin.y + i * ray.origin.z + j;

		if (solveQuadratic(Aq, Bq, Cq, t0, t1))
		{
			if (t1 > 0.0f)
			{
				if (t0 < 0.0f)
				{
					if (t1 < t)
					{
						t = t1;
						retVal = INPRIM;
					}
				}
				else
				{
					if (t0 < t)
					{
						t = t0;
						retVal = HIT;
					}
				}
			}
		}

		return retVal;
	}

	bool solveQuadratic(const float & a, const float & b, const float & c, float & t0, float & t1)
	{
		if (a == 0.0f)
		{
			t0 = t1 = -c / b;
			return true;
		}

		float discr = b * b - 4.0f * a * c;
		if (discr < 0.0f) return false;

		discr = glm::sqrt(discr);
		t0 = (-b - discr) / 2.0f * a;
		t1 = (-b + discr) / 2.0f * a;

		if (t0 > t1) std::swap(t0, t1); 

		return true;
	}

	glm::vec3 normal(const glm::vec3 & position, const glm::vec3 & direction) override
	{
		float xn = 2.0f * a * position.x + d * position.y + e * position.z + g;
		float yn = 2.0f * b * position.y + d * position.x + f * position.z + h;
		float zn = 2.0f * c * position.z + e * position.x + f * position.y + i;

		glm::vec3 n = glm::vec3(xn, yn, zn);
		n = glm::normalize(n);
		n = glm::dot(n, direction) > (0.0f + EPSILON) ? -n : n;

		return n;
	}

	void translate(const glm::vec3 & t)
	{
		translate(t.x, t.y, t.z);
	}

	void translate(float x, float y, float z)
	{
		j += a * x * x + b * y * y + c * z * z + f * y * z + e * x * z + d * x * y - g * x - h * y - i * z;
		g += -2.0f * a * x - e * z - d * y;
		h += -2.0f * b * y - f * z - d * x;
		i += -2.0f * c * z - f * y - e * x;
	}
};

struct QuadricEllipsoid : QuadricGeneric
{
	QuadricEllipsoid(float a_aS, float a_bS, float a_cS, float a_j) : QuadricGeneric(1.0f / a_aS, 1.0f / a_bS, 1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, a_j) { }
	int getType() override { return QUADRIC_ELLIPSOID; }
};

struct QuadricParaboloidX : QuadricGeneric
{
	QuadricParaboloidX(float a_bS, float a_cS) : QuadricGeneric(0.0f, 1.0f / a_bS, 1.0f / a_cS, 0.0f, 0.0f, 0.0f, -2.0f, 0.0f, 0.0f, 0.0f) { }
	int getType() override { return QUADRIC_PARABOLOID; }
};

struct QuadricParaboloidY : QuadricGeneric
{
	QuadricParaboloidY(float a_aS, float a_cS) : QuadricGeneric(1.0f / a_aS, 0.0f, 1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, -2.0f, 0.0f, 0.0f) { }
	int getType() override { return QUADRIC_PARABOLOID; }
};

struct QuadricParaboloidZ : QuadricGeneric
{
	QuadricParaboloidZ(float a_aS, float a_bS) : QuadricGeneric(1.0f / a_aS, 1.0f / a_bS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -2.0f, 0.0f) { }
	int getType() override { return QUADRIC_PARABOLOID; }
};

struct QuadricHyperboloid : QuadricGeneric
{
	QuadricHyperboloid(float a_aS, float a_bS, float a_cS, float a_j) : QuadricGeneric(1.0f / a_aS, 1.0f / a_bS, -1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, a_j) { }
	int getType() override { return QUADRIC_HYPERBOLOID; }
};

struct QuadricCone : QuadricGeneric
{
	QuadricCone(float a_aS, float a_bS, float a_cS) : QuadricGeneric(1.0f / a_aS, 1.0f / a_bS, -1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f) { }
	int getType() override { return QUADRIC_CONE; }
};

struct QuadricCylinderX : QuadricGeneric
{
	QuadricCylinderX(float a_bS, float a_cS, float a_j) : QuadricGeneric(0.0f, 1.0f / a_bS, 1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, a_j) { }
	int getType() override { return QUADRIC_CYLINDER; }
};

struct QuadricCylinderY : QuadricGeneric
{
	QuadricCylinderY(float a_aS, float a_cS, float a_j) : QuadricGeneric(1.0f / a_aS, 0.0f, 1.0f / a_cS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, a_j) { }
	int getType() override { return QUADRIC_CYLINDER; }
};

struct QuadricCylinderZ : QuadricGeneric
{
	QuadricCylinderZ(float a_abS, float a_j) : QuadricGeneric(1.0f / a_abS, 1.0f / a_abS, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, a_j) { }
	int getType() override { return QUADRIC_CYLINDER; }
};