 #pragma once

struct Material
{
	glm::vec3 color;
	float reflection;
	float diffuse;
	float specular;
	float refraction;
	float refractionIndex;
};