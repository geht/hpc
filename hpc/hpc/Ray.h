#pragma once

struct Ray
{
	glm::vec3 origin;
	glm::vec3 direction; // should be normalized

	glm::vec3 evaluate(float t)
	{
		return origin + direction * t;
	}
};
