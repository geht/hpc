#pragma once

struct Scene
{
	int numPrimitives;
	Primitive ** primitives;

	Scene()
	{
		printf("setting up the scene...\n");
		numPrimitives = 7;
		primitives = new Primitive*[numPrimitives];

		printf("setting up %i primitives...\n", numPrimitives);

		// ground plane
		primitives[0] = new Plane(glm::vec3(0.0f, 1.0f, 0.0f), 4.4f);
		primitives[0]->name = "ground plane";
		primitives[0]->material.color = glm::vec3(0.4f, 0.3f, 0.3f);
		primitives[0]->material.reflection = 0.0f;
		primitives[0]->material.diffuse = 1.0f;
		primitives[0]->material.specular = 0.0f;
		primitives[0]->material.refraction = 0.0f;

		// big sphere
		//primitives[1] = new Sphere(1.25f, -0.8f, 2.0f, 2.5f);
		primitives[1] = new Sphere(4.75f, -0.8f, 2.0f, 2.5f);
		primitives[1]->name = "big sphere";
		primitives[1]->material.color = glm::vec3(0.7f, 0.7f, 1.0f);
		primitives[1]->material.reflection = 0.2f;
		primitives[1]->material.diffuse = 0.0f;
		primitives[1]->material.specular = 1.0f;
		primitives[1]->material.refraction = 0.8f;
		primitives[1]->material.refractionIndex = 1.3f;

		// small sphere
		primitives[2] = new Sphere(-6.5f, -0.5f, 7.0f, 2.0f);
		primitives[2]->name = "small sphere";
		primitives[2]->material.color = glm::vec3(0.7f, 0.7f, 1.0f);
		primitives[2]->material.reflection = 1.0f;
		primitives[2]->material.diffuse = 0.1f;
		primitives[2]->material.specular = 0.9f;
		primitives[2]->material.refraction = 0.0f;
		primitives[2]->material.refractionIndex = 1.3f;

		// light source one
		primitives[3] = new Sphere(0.0f, 5.0f, 5.0f, 0.1f);
		primitives[3]->name = "light one";
		primitives[3]->isLight = true;
		primitives[3]->material.color = glm::vec3(0.6f, 0.6f, 0.6f);

		// light source two
		primitives[4] = new Sphere(2.0f, 5.0f, 1.0f, 0.1f);
		primitives[4]->name = "light two";
		primitives[4]->isLight = true;
		primitives[4]->material.color = glm::vec3(0.7f, 0.7f, 0.9f);

		// small sphere two
		primitives[5] = new Sphere(5.5f, -0.5f, 8.0f, 2.0f);
		primitives[5]->name = "small sphere two";
		primitives[5]->material.color = glm::vec3(0.0f, 0.0f, 1.0f);
		primitives[5]->material.reflection = 0.1f;
		primitives[5]->material.diffuse = 0.8f;
		primitives[5]->material.specular = 0.2f;
		primitives[5]->material.refraction = 0.0f;
		primitives[5]->material.refractionIndex = 1.3f;

		// quadric
		// QuadricGeneric * q = new QuadricGeneric(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f); // sphere
		// QuadricGeneric * q = new QuadricGeneric(1.0f, 4.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -0.25f); // small ellipsoid
		// QuadricGeneric * q = new QuadricGeneric(0.5f * 0.5f, 0.3f * 0.3f, 0.25f * 0.25f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -0.25f); // ellipsoid
		// QuadricGeneric * q = new QuadricGeneric(1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f); // cylinder
		
		// QuadricGeneric * q = new QuadricEllipsoid(1.0f, 1.0f, 1.0f, -2.0f); // sphere
		// QuadricGeneric * q = new QuadricEllipsoid(5.0f, 2.0f, 1.0f, -2.0f); // ellipsoid
		// QuadricGeneric * q = new QuadricParaboloidY(-1.0f, -1.0f); // paraboloid along -y-axis
		// QuadricGeneric * q = new QuadricHyperboloid(-1.0f, 1.0f, -1.0f, 1.0f); // two-sheet hyperboloid along x-axis
		// QuadricGeneric * q = new QuadricCone(-1.0f, 1.0f, -1.0f); // cone along x-axis
		// QuadricGeneric * q = new QuadricCylinderY(1.0f, 1.0f, -0.5f); // cylinder along y-axis

		QuadricGeneric * q = new QuadricParaboloidY(-1.0f, -1.0f); // paraboloid along -y-axis

		q->translate(0.0f, 0.0f, 7.0f);
		primitives[6] = q;
		primitives[6]->name = "generic quadric";
		primitives[6]->material.color = glm::vec3(1.0f, 0.0f, 1.0f);
		primitives[6]->material.reflection = 0.0f;
		primitives[6]->material.diffuse = 1.0f;
		primitives[6]->material.specular = 0.6f;
		primitives[6]->material.refraction = 0.0f;

		for (int i = 0; i < numPrimitives; i++)
		{
			printf("set up a %s\n", primitives[i]->name);
		}

		printf("scene is set up\n\n");
	}

	~Scene()
	{
		delete primitives;
	}
};