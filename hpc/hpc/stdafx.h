// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// defines

// constants
const float EPSILON = 0.001f;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SUPER_SAMPLES = 1; // spawn SUPER_SAMPLES * SUPER_SAMPLES rays per pixel
const int MAX_TRACE_DEPTH = 6;

const char * const FILE_PATH = "../hpc.ppm";

const int HIT =		1; // ray hit a primitive
const int MISS =	0; // ray missed
const int INPRIM = -1; // ray started inside of a primitive

#include "targetver.h"

// TODO: reference additional headers your program requires here

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <chrono>

// include glm
#include <glm\glm.hpp>

// include ITBB stuff
#include <tbb\parallel_for.h>
#include <tbb\blocked_range2d.h>
#include <tbb\partitioner.h>

#include "Ray.h"
#include "Material.h"
#include "Primitive.h"
#include "Quadrics.h"
#include "Plane.h"
#include "Sphere.h"
#include "Scene.h"
#include "ScreenPlane.h"
#include "RayTracer.h"
