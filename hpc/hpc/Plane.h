#pragma once

struct Plane : Primitive
{
	glm::vec3 N;
	float D;

	Plane(const glm::vec3 & a_normal, float a_d) :
		N(a_normal), D(a_d)
	{
		isLight = false;
	}

	int getType() override { return PLANE; }

	int intersect(const Ray & ray, float & t) override
	{
		int retVal = MISS;

		float d = glm::dot(N, ray.direction);
		if (d == 0.0f) return retVal;

		float dist = -(glm::dot(N, ray.direction) + D) / d;
		if (dist > 0.0f && dist < t)
		{
			t = dist;
			retVal = HIT;
		}

		return retVal;
	}

	glm::vec3 normal(const glm::vec3 & position, const glm::vec3 & direction) override
	{
		return N;
	}
};