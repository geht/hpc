#pragma once

struct Primitive
{
	enum
	{
		PLANE =   0,
		SPHERE =  1,
		QUADRIC_GENERIC = 2,
		QUADRIC_ELLIPSOID = 3,
		QUADRIC_PARABOLOID = 4,
		QUADRIC_HYPERBOLOID = 5,
		QUADRIC_CONE = 6,
		QUADRIC_CYLINDER = 7,
	};

	char * name;
	bool isLight;
	Material material;

	virtual int getType() = 0;
	virtual int intersect(const Ray & ray, float & t) = 0;
	virtual glm::vec3 normal(const glm::vec3 & position, const glm::vec3 & direction) = 0;
};